#include "controller.h"
#include <gmParametricsModule>
Controller::Controller()
{

}

Controller::~Controller()
{

}
Controller::Controller(GMlib::Scene *sc)
{
    this->sc=sc;

}

void Controller:: insertscene(){

    GMlib::DMatrix<double> value(mat(3 , 0.3 , 1));

    std::cout<< "text" <<value;
    std::cout.flush();

//        firstc = new GMlib::Mycurve<float>;
//        firstc -> toggleDefaultVisualizer();
//       firstc->rotate(75, GMlib::Vector<float,3>(0,0,1));

//        firstc->replot(1000,1);

//        sc->insert(firstc);


//        secondc = new GMlib::Mycurve<float>;
//        secondc -> toggleDefaultVisualizer();
//        secondc->replot(10000,1);
//      secondc->rotate(90, GMlib::Vector<float,3>(0,0,1));

//        sc->insert(secondc);

//        thirdc = new GMlib::Mycurve<float>;
//        thirdc -> toggleDefaultVisualizer();
//        thirdc->replot(1000,1);
//       thirdc->rotate(45, GMlib::Vector<float,3>(1,0,0));

//        sc->insert(thirdc);

//        fourthc = new GMlib::Mycurve<float>;
//        fourthc -> toggleDefaultVisualizer();
//        fourthc->replot(1000,1);
//        fourthc->rotate(67.5, GMlib::Vector<float,3>(0,0,1));

//        sc->insert(fourthc);

//        fivc = new GMlib::Mycurve<float>;
//        fivc -> toggleDefaultVisualizer();
//        fivc->replot(1000,1);
//        fivc->rotate(90, GMlib::Vector<float,3>(0,0,1));

//       sc->insert(fivc);

//        sixc = new GMlib::Mycurve<float>;
//        sixc -> toggleDefaultVisualizer();
//        sixc->replot(1000,1);
//        sixc->rotate(112.5, GMlib::Vector<float,3>(0,0,1));

//        sc->insert(sixc);

//        sevc = new GMlib::Mycurve<float>;
//        sevc -> toggleDefaultVisualizer();
//        sevc->replot(1000,1);
//       sevc->rotate(270, GMlib::Vector<float,3>(0,0,1));

//        sc->insert(sevc);

    eighthc = new GMlib::Mycurve<float>;
    eighthc -> toggleDefaultVisualizer();
    eighthc->replot(1000,1);
    eighthc->rotate(135, GMlib::Vector<float,3>(0,0,1));
   //sc->insert(eighthc);


//    sub = new SubCurve(eighthc,0.2, 0.8);
//    sub -> toggleDefaultVisualizer();
//    sub->replot(1000,1);
//    sub->setColor(GMlib::GMcolor::Green);
//    sub->translate(GMlib::Vector<float,3>(0,0,10));
//    sc->insert(sub);

    erb = new MyErbsCurve(eighthc,5);
    erb->toggleDefaultVisualizer();
    erb->replot(98,1);
    erb->setColor(GMlib::GMcolor::Blue);
    erb->translate(GMlib::Vector<float,3>(1,0,0));
    sc->insert(erb);

//    bezier=new MyBezierCurve (eighthc,1,2,3,4);
//    bezier->toggleDefaultVisualizer();
//    bezier->replot(1000,1);
//    bezier->setColor(GMlib::GMcolor::Aqua);
//    bezier->translate(GMlib::Vector<float,3>(1,0,0));
//    sc->insert(bezier);


//    KnotVector knots(10,-4,4);
//    std::cout<< "knot" <<knots;
//    std::cout.flush();



}

GMlib::DMatrix<double> Controller::mat(int d, double t, double scale)
{
    GMlib::DMatrix<double> B(d+1,d+1);
    B[d-1][0] = 1-t;
    B[d-1][1] = t;
    for(int i = d-2; i >= 0; i--)
    {
        B[i][0] = (1-t)*B[i+1][0];
        for(int j=1; j < d-i; j++)
        {
            B[i][j] = t * B[i+1][j-1] + (1-t) * B[i+1][j];
        }
        B[i][d-i] = t * B[i+1][d-i-1];
    }

    B[d][0] = -scale;
    B[d][1] = scale;
    for(int k=2; k <= d; k++)
    {
        double s = k * scale;
        for(int i = d; i > d-k; i--)
        {
            B[i][k] = s * B[i][k-1];
            for(int j = k-1; j > 0; j--)
            {
                B[i][j] = s * (B[i][j-1] - B[i][j]);
            }
            B[i][0] = -s * B[i][0];
        }
    }

    return B;
}
void       Controller::eval(float t, int d, bool l ){

}
float      Controller::getEndP(){

}
float      Controller::getStartP(){

}
