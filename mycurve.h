#ifndef MYCURVE_H
#define MYCURVE_H


#include<gmParametricsModule>


namespace GMlib {


  template <typename T>
  class Mycurve : public PCurve<T,3> {
    GM_SCENEOBJECT(Mycurve)
  public:
    Mycurve( T size = T(5) );
    Mycurve( const Mycurve<T>& copy );
    virtual ~Mycurve();

    bool          isClosed() const;


  protected:
    T             _size;

    void	        eval(T t, int d, bool l);
    T             getEndP();
    T             getStartP();
};
}

#include "mycurve.c"
#endif MYCURVE_H
