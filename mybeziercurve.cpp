#include "mybeziercurve.h"




MyBezierCurve::MyBezierCurve(GMlib::PCurve<float,3>* curve,float start, float end,float t, int d){

    _closed = curve->isClosed();
    _curve  = curve;
    _start  = start;
    _end    = end;
    _t      = t ;
    _d      = d;
    Controlpt(t,d);
}


MyBezierCurve::~MyBezierCurve()

{
    delete _curve;
}

void MyBezierCurve::eval(float t, int d, bool){
    this->_p.setDim(1 + _d);  // ??? the same (1+d) behind line
    GMlib::DMatrix<double> m = mat(_d, t, 1/(_end - _start));
    this->_p = m * _controlpoint;
    this->_p.setDim(1 + d); //???
}

float MyBezierCurve::getStartP(){
    return 0.0;
}


float MyBezierCurve::getEndP(){
    return 1.0;
}


GMlib::DMatrix<double> MyBezierCurve::mat(int d, double t, double scale)
{


    GMlib::DMatrix<double> B(d+1,d+1);
    B[d-1][0] = 1-t;
    B[d-1][1] = t;
    for(int i = d-2; i >= 0; i--)
    {
        B[i][0] = (1-t)*B[i+1][0];
        for(int j=1; j < d-i; j++)
        {
            B[i][j] = t * B[i+1][j-1] + (1-t) * B[i+1][j];
        }
        B[i][d-i] = t * B[i+1][d-i-1];
    }

    B[d][0] = -scale;
    B[d][1] = scale;
    for(int k=2; k <= d; k++)
    {
        double s = k * scale;
        for(int i = d; i > d-k; i--)
        {
            B[i][k] = s * B[i][k-1];
            for(int j = k-1; j > 0; j--)
            {
                B[i][j] = s * (B[i][j-1] - B[i][j]);
            }
            B[i][0] = -s * B[i][0];
        }
    }

    return B;
}

void MyBezierCurve::Controlpt(float t, int d){

    int scale = 1.0/(_end - _start);
    GMlib::DMatrix<double>  m = this->mat(d,t,scale).invert();
    std::cout<<"t : "<<t<<std::endl;
    std::cout<<"matrix : "<<this->mat(d,t,scale)<<std::endl;
    std::cout<<"invert matrix m: "<<m<<std::endl;
    GMlib::DVector<GMlib::Vector<float,3> > g = _curve->evaluateParent(t,d);
    std::cout<<"g: "<<g<<std::endl;
    _controlpoint = m * g;

    std::cout<<"control point of bezier: "<<_controlpoint<<std::endl;

}

bool MyBezierCurve::isClosed(){
    return _closed;
}

