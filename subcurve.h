#ifndef SUBCURVE_H
#define SUBCURVE_H
#include "gmParametricsModule"
class SubCurve : public GMlib::PCurve<float,3>{
    GM_SCENEOBJECT(SubCurve)

    public:
        SubCurve();
    ~SubCurve();
    SubCurve( GMlib::PCurve<float,3>* curve,float start, float end);
    SubCurve( GMlib::PCurve<float,3>* curve,float start, float end, float t);
    bool            isClosed() const;


    SubCurve(const SubCurve &copy );

private:


    void set(GMlib::PCurve<float,3>* curve, float start, float end, float t);
protected:

    void  eval(float t, int d, bool l = true );
    GMlib::PCurve<float,3>*     curve_;
    float                       start_;
    float                       t_;
    float                       end_;
    GMlib::Vector<float,3>      translate_;

    // virtual functions from PSurf

    float                   getEndP();
    float                   getStartP();

};

#endif // SUBCURVE_H
