#include "knotvector.h"

#include  "qdebug.h"

KnotVector::KnotVector(int n,  float start,float end )

{
   create( n, start,end);
}


KnotVector::~KnotVector()
{

}

void KnotVector::create(int n,  float start, float end){

     float deltasize = (end -start)/(n -1);

    (*this).setDim(n+2);



        for(int i = 0; i < n; i++)
    {
        (*this)[i+1] = start + i * deltasize;
        (*this)[0]=start;
        (*this)[n]=(*this)[n+1]=end;



    }

}

float KnotVector::getStart(){
    return(*this)[1]
            ;
}

float KnotVector::getEnd(){
return(*this)(getDim()-2);
}

