#include "mycurve.h"

#include <math.h>


namespace GMlib {


template <typename T>
inline
Mycurve<T>::Mycurve( T size ) {

    _size = size;
    this->_dm = GM_DERIVATION_EXPLICIT;
}


template <typename T>
inline
Mycurve<T>::Mycurve( const Mycurve<T>& copy ) : PCurve<T,3>( copy ) {}


template <typename T>
Mycurve<T>::~Mycurve() {}


template <typename T>
inline
void Mycurve<T>::eval( T t, int d, bool /*l*/ ) {

    this->_p.setDim( d + 1 );

    this->_p[0][0] = _size * T(4*(sin(0.56*t))*sin(0.56*t)/(1+std::pow(2,pow(cos(2.02*t),2))));
    this->_p[0][1] = _size * T(7*((sin(0.56*t)/(1+std::pow(sin((2.02)*t),2)))));
    this->_p[0][2] = float (0);

//    this->_p[0][0] = _size * T(sin(t));
//    this->_p[0][1] = _size * T(cos(t));
//    this->_p[0][2] = float (0);

    this->_p[0][0] = _size * T(4.5*(sin(t))-sin(3*t)+(0.8*sin(15.25*t)));
    this->_p[0][1] = _size * T(4*cos(t)- (1.5*cos(2*t))-(0.6*cos(3*t))+(0.8*cos(15.25*t)));
    this->_p[0][2] = float (0);

//    if (d>0)
//    {
//        this->_p[1][0] = _size * T(9*(cos(t)/2)-cos(3*t)*3+(61*cos(15.25*t)/5));
//        this->_p[1][1] = _size * T(61*sin(15.25*t)/5+ (3*sin(2*t))+(9*sin(3*t)/5)-(4*sin(t)));
//        this->_p[1][2] = float (0);
//    }



}
    template <typename T>
    inline
            T Mycurve<T>::getEndP() {

        return T( 6);
    }


    template <typename T>
    inline
            T Mycurve<T>::getStartP() {

        return T(-6);
    }


    template <typename T>
    inline
            bool Mycurve<T>::isClosed() const {

        return true;
    }
}





