#include "mycurve.h"


namespace GMlib {


template <typename T>
inline
Mycurve<T>::Mycurve( T size ) {

    _size = size;
    this->_dm = GM_DERIVATION_EXPLICIT;
}


template <typename T>
inline
Mycurve<T>::Mycurve( const Mycurve<T>& copy ) : PCurve<T,3>( copy ) {}


template <typename T>
Mycurve<T>::~Mycurve() {}


template <typename T>
inline
void Mycurve<T>::eval( T t, int d, bool /*l*/ ) {

    this->_p.setDim( d + 1 );


    this->_p[0][0] = _size * T(7*(cos(0.56*t))*sin(0.56*t)/(1+std::pow(2,pow(cos(2.02*t),2))));
    this->_p[0][1] = _size * T(7*((sin(0.56*t)/(1+std::pow(sin((2.02)*t),2)))));
    this->_p[0][2] = float (0);




}
    template <typename T>
    inline
            T Mycurve<T>::getEndP() {

        return T( 3 );
    }


    template <typename T>
    inline
            T Mycurve<T>::getStartP() {

        return T(-3);
    }


    template <typename T>
    inline
            bool Mycurve<T>::isClosed() const {

        return true;
    }
}



