#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <gmParametricsModule>
#include "mycurve.h"
#include "knotvector.h"
#include "subcurve.h"
#include "mybeziercurve.h"
#include "erbsapp.h"
#include "gmParametricsModule"
//#include <parametrics/gmpcurve>

class Controller : public GMlib::PCurve<float,3> {

    GM_SCENEOBJECT(Controller)

public:
    Controller();
    ~Controller();
    Controller(GMlib::Scene *sc);
    void insertscene();


  protected:

  //    void localSimulate(double dt);

  private:
      GMlib::Scene *sc;
      GMlib::DMatrix<double> mat(int d, double t, double scale);
    GMlib::Mycurve<float>* firstc;
    GMlib::Mycurve<float>* secondc;
    GMlib::Mycurve<float>* thirdc;
    GMlib::Mycurve<float>* fourthc;
    GMlib::Mycurve<float>* fivc;
    GMlib::Mycurve<float>* sixc;
    GMlib::Mycurve<float>* sevc;
    GMlib::Mycurve<float>* eighthc;
    SubCurve* sub;
    MyErbsCurve* erb;
    MyBezierCurve* bezier;

    GMlib::Mycurve<float>* fig;
      void           eval(float t, int d, bool l = true );
      float          getEndP();
      float          getStartP();

};

#endif // CONTROLLER_H
