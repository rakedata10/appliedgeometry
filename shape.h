#ifndef SHAPE_H
#define SHAPE_H
#include"gmParametricsModule"


namespace GMlib {


  template <typename T>
  class shape : public PCurve<T,3> {
    GM_SCENEOBJECT(shape)
  public:
    shape( T size = T(5) );
    shape( const shape<T>& copy );
    virtual ~shape();

    bool          isClosed() const;


  protected:
    T             _size;

    void	        eval(T t, int d, bool l);
    T             getEndP();
    T             getStartP();
};
}

#include "shape.c"
#endif SHAPE_H
