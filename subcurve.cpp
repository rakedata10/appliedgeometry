#include "subcurve.h"

SubCurve::SubCurve()
{

}

SubCurve::~SubCurve()
{

}

SubCurve::SubCurve( GMlib::PCurve<float,3>* curve, float start, float end )


{
    (*this)._dm=GMlib::GM_DERIVATION_EXPLICIT; // _dm derivative method


    set(curve, start, end, (end + start)/2);

    GMlib::DVector<GMlib::Vector<float,3> > tr = curve_->evaluateParent(t_, 0);
    translate_ = tr[0];
    this->translateParent( translate_ );


}

SubCurve::SubCurve( GMlib::PCurve<float,3>* curve,float start, float end, float t )
{
    (*this)._dm=GMlib::GM_DERIVATION_EXPLICIT;

    set(curve, start, end, t);

    GMlib::DVector< GMlib::Vector<float,3> > tr = curve_->evaluateParent(t_, 0);
    translate_ = tr[0];
    this->translateParent( translate_ );
}



SubCurve::SubCurve( const SubCurve& copy ) : GMlib::PCurve<float,3>( copy )
{
    set(copy.curve_, copy.start_, copy.end_, copy.t_);

    translate_ = copy.translate_;
}


void SubCurve::eval( float t, int d, bool /*l*/ )
{
    this->_p     = curve_->evaluateParent(t , d);
    this->_p[0] -= translate_;
}



float SubCurve::getStartP()
{
    return start_;
}



float SubCurve::getEndP()
{
    return end_;
}



bool SubCurve::isClosed() const
{
    return false;
}


void SubCurve::set(GMlib::PCurve<float,3>* curve, float start, float end, float t)
{
    curve_ = curve;
    start_ = start;
    t_ = t;
    end_ = end;
}
