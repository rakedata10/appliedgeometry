#include "erbsapp.h"


#include <iomanip>



MyErbsCurve::MyErbsCurve(GMlib::PCurve<float,3> *b, int n)
{
    _n = n;
    _knot.setDim(n+2);
    _start = b->getParStart();
    _end   = b->getParEnd();
    _closed = b->isClosed();
    KnotVector(_start,_end);
    std::cout<< "knot" <<_knot << std::endl;
    std::cout.flush();

     this->LocalSubCurve(b);
//    for (int i=0;i<_curveV.getDim();i++)
//    std::cout<< "lc[" <<i << "] - " << _curveV[i]->getParStart() << " , " << _curveV[i]->getParEnd() << std::endl;
//    std::cout.flush();

}

MyErbsCurve::MyErbsCurve(GMlib::PCurve<float,3> *b, int n, int d)
{
     _n = n;
    _knot.setDim(n + 2);
    _start = b->getParStart();
    _end   = b->getParEnd();
    this->KnotVector(_start,_end);
   this->LocalBezierCurve(b,d);
}
void MyErbsCurve::LocalSubCurve(GMlib::PCurve<float,3> *b){
    _curveV.setDim(_n);
    for(int i = 1; i < _n; i++){

        _curveV[i-1]= new GMlib::PSubCurve<float>(b,_knot[i-1],_knot[i+1],_knot[i]);
        _curveV[i-1]->toggleDefaultVisualizer();
         _curveV[i-1]->setColor(GMlib::GMcolor::YellowGreen);
         _curveV[i-1]->replot(100,1);

         // if (i==5)
           this->insert(_curveV[i-1]);
    }
    if(isClosed())
        _curveV[_n-1]= _curveV[0];

    else {
        _curveV[_n-1]= new GMlib::PSubCurve<float>(b,_knot[_n],_knot[_n+2],_knot[_n+1]);
        _curveV[_n-1]->toggleDefaultVisualizer();
         _curveV[_n-1]->setColor(GMlib::GMcolor::YellowGreen);
     _curveV[_n-1]->replot(100,1);
     this->insert(_curveV[_n-1]);
    }
}

void MyErbsCurve::LocalBezierCurve(GMlib::PCurve<float,3> *b,int d){

    _curveV.setDim(_n);
    for(int i = 0; i < _n;i++)
        _curveV[i-1]= new MyBezierCurve(b,_knot[i-1],_knot[i+1],_knot[i],d);

    if(isClosed())
        _curveV[_n-1]= _curveV[0];

    else
        _curveV[_n-1]= new MyBezierCurve(b,_knot[_n-1],_knot[_n+1],_knot[_n],d);
}

void MyErbsCurve::KnotVector(float start, float end){

    float delta = (end - start)/(_n-1);
    _knot[0] = _knot[1] = _start;

    for(int i = 1;i < _n-1; i++ )
        _knot[i+1] = _knot[i]+ delta;

    _knot[_n] = _knot[_n+1] = end;


    if(isClosed()){

        _knot[0] = _knot[1] -  (_knot[_n] - _knot[_n-1]);
        _knot[_n+1] = _knot[_n] + (_knot[2] - _knot[1]);

    }


}

MyErbsCurve::MyErbsCurve(const MyErbsCurve &copy){

    _knot         = copy._knot;
    _start        = copy._start;
    _end          = copy._end;
    _n            = copy._n;

}
MyErbsCurve::~MyErbsCurve(){

}
int MyErbsCurve::findingIndex(float t){

    for(int i = 1; i < _knot.getDim()-2; ++i){
       if(t >= _knot[i] && t < _knot[i+1]  )

            return i;
}
   return _knot.getDim()-3;

}




void MyErbsCurve::eval(float t, int d, bool){

    int  indx = findingIndex(t);
    float w = ((t - _knot[indx])/(_knot[indx + 1] - _knot[indx]))
            ;
//    float scale = 1.0;
    GMlib::DVector<float> B = BFunction(w,d);
    GMlib::DVector< GMlib::Vector<float,3>>  c1 = _curveV[indx-1]->evaluateParent(t,d);
    GMlib::DVector< GMlib::Vector<float,3>>  c2 = _curveV[indx]->evaluateParent(t,d);

//    std::cout << std::setw(10) << std::setprecision(7) << "t" << std::setw(2) << "i" << std::endl;
//    std::cout << std::setw(10) << std::setprecision(7) << t << std::setw(2) << indx << std::endl;
//    std::cout<<"t="<<t<<std::endl;
//    std::cout<<"index="<<indx<<std::endl;
//    std::cout<<"c1 ="<<c1<<std::endl;
//    std::cout<<"c2 ="<<c2<<std::endl;


    _p[0] = c1[0]+ B[0]*( c2[0] -  c1[0]);

    if( d > 0)
        _p[1] = c1[1]+ B[0]*( c2[1] -  c1[1])
                + B[1]*( c2[0] -  c1[0]) ;

    if( d > 1)
        _p[2] = c1[0]+ B[0]*( c2[2] -  c1[2])
                + 2*B[1]*( c2[1] -  c1[1])
                + B[2]*( c2[0] -  c1[0]) ;



}
GMlib::DVector<float> MyErbsCurve::BFunction(float t, int d){ //d :degree, scale: from 0 to 1


    GMlib::DVector<float> b(d+1);

    // b[0] : formula
    b[0] = 3*std::pow(t,2) - 2*std::pow(t,3);

    if(d > 0)
        b[1] = (6*t-6*std::pow(t,2)) ;
    if (d > 1)
        b[2] =(6-12*t );
    if(d > 2)
        b[3] = -12 ;

    return b;

}



float MyErbsCurve::getEndP(){

    return _knot[_knot.getDim()-2];
}

float MyErbsCurve::getStartP(){

    return _knot[1];
}

bool MyErbsCurve::isClosed() const
{
    return _closed;
}

void MyErbsCurve::localSimulate(double dt)
{
//    for (int i = 0; i < _curveV.getDim()-1; i++) {


//        _curveV[4]->rotate(dt,GMlib::Vector<float,3>(1,1,1));
//        this->replot(100,1);
//}
    }
