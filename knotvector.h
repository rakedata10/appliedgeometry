#ifndef KNOTVECTOR_H
#define KNOTVECTOR_H

#include <gmCoreModule>
#include <gmParametricsModule>

class KnotVector : public GMlib::DVector<float>

{
public:
    KnotVector(int n,  float start,float end);
    ~KnotVector();

    float getStart();
    float getEnd();


private:

    void create(int n, float start,float end);


};

#endif // KNOTVECTOR_H
