#ifndef MYBEZIERCURVE_H

#define MYBEZIERCURVE_H

#include <gmParametricsModule>



class MyBezierCurve : public GMlib::PCurve<float,3>{
    GM_SCENEOBJECT(MyBezierCurve)

private:

 GMlib::DMatrix<double> mat(int d, double t, double scale);
public:

    ~MyBezierCurve();
    bool isClosed();

    MyBezierCurve(GMlib::PCurve<float,3> *curve, float start, float end, float t, int d);
protected:
   GMlib::DVector<GMlib::Vector<float,3> > _controlpoint;
   GMlib::PCurve<float,3>*                 _curve;
    float                                  _start;
    float                                  _t;
    float                                  _end;
    bool                                   _closed;
    int                                    _d;


    float getStartP();
    float getEndP();
    void  eval(float t, int d, bool l = true ) ;


    GMlib::DMatrix<float> calcBernsteinPolynomial( int d, float t, float scale ); // bernsteinHemite property
    void Controlpt(float t, int d);

};



#endif // MYBEZIERCURVE_H

