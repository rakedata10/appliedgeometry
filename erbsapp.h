#ifndef ERBSAPP_H
#define ERBSAPP_H
#include "subcurve.h"
#include "mybeziercurve.h"
#include "gmParametricsModule"


class MyErbsCurve : public GMlib::PCurve<float,3>{
    GM_SCENEOBJECT(MyErbsCurve)

    private:
    GMlib::DVector<float> BFunction(float t,int d);

    GMlib::DVector<float>                           _knot;
    GMlib::DVector<GMlib::PCurve<float,3> *>        _curveV;
    int                                             _n;
    float                                           _start;
    bool                                            _closed;
    float                                           _end;

public:

    MyErbsCurve(GMlib::PCurve<float,3> *b,int n);
    MyErbsCurve(GMlib::PCurve<float,3> *b,int n,int d);
    MyErbsCurve(const MyErbsCurve &copy);
    MyErbsCurve();

    ~MyErbsCurve();


    bool  isClosed() const;




protected:
    void  eval( float t, int d, bool=1) ;
    float getStartP();
    float getEndP();

    void KnotVector(float start, float end);
    void LocalSubCurve(GMlib::PCurve<float,3> *b);
    void LocalBezierCurve(GMlib::PCurve<float,3> *b, int d);
    int  findingIndex(float t);

    //virtual fun inherited frm sceneobj
    void localSimulate(double dt);

   int findingIndex(float t, const MyErbsCurve &knotvector, bool closed);
};



#endif // ERBSAPP_H
